Glossary
========

.. glossary::

  NCBI
    National Center for Biotechnology Information, https://www.ncbi.nlm.nih.gov

  E-Utilities
  E-Utility
    Collection of :term:`NCBI` tools handling queries to :term:`Entrez`

  Entrez
    :term:`NCBI` database servers storing biomedical data and literature

  UID
  UIDs
    Document identifier unique within one :term:`Entrez` database

  source database
    The database from which UIDs are linkd from

  target database
    The database from which UIDs are linked

  WebEnv
    String referencing a E-Utility query

  querykey
  query_key
    Number referencing a specific request for a :term:`WebEnv`

  Entrezpy query
  Entrezpy querys
  entrezpy query
  entrezpy querys
    A query to onn E-Utility function in entrezpy is considered one query,
    which can have several :term:`entrezpy requests`.

  Entrezpy request
  Entrezpy requests
  entrezpy request
  entrezpy requests
    One request as part of an :term:`entrezpy query`.
