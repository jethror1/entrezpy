.. _request_mods:

Requester module
================

Requester
---------
  .. automodule:: entrezpy.requester.requester
    :members:
    :undoc-members:
