.. _efetch_mods:

Efetch modules
==============

Efetcher
--------
  .. inheritance-diagram:: entrezpy.efetch.efetcher
  .. automodule:: entrezpy.efetch.efetcher
    :members:
    :undoc-members:
    :show-inheritance:

EfetchParameter
----------------
  .. inheritance-diagram:: entrezpy.efetch.efetch_parameter
  .. automodule:: entrezpy.efetch.efetch_parameter
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:

EfetchAnalyzer
---------------
  .. inheritance-diagram:: entrezpy.efetch.efetch_analyzer
  .. automodule:: entrezpy.efetch.efetch_analyzer
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:

EfetchRequest
--------------
  .. inheritance-diagram:: entrezpy.efetch.efetch_request
  .. automodule:: entrezpy.efetch.efetch_request
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
